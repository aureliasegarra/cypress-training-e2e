describe('Password test', () => {
	beforeEach(() => {
		cy.visit('http://zero.webappsecurity.com/');
	});

	it('Should click on the signin button', () => {
		cy.get('#signin_button').click();
	});

	it('Should click on the forgotten password', () => {
        cy.get('#signin_button').click();
		cy.get('.offset3 > a').click();
	});

	it('Should fill email form', () => {
        cy.get('#signin_button').click();
		cy.get('.offset3 > a').click();
		cy.get('#user_email').type('testemail@email.com');
	});

	it('Should submit the form', () => {
        cy.get('#signin_button').click();
		cy.get('.offset3 > a').click();
		cy.get('#user_email').type('testemail@email.com');
		cy.contains('Send Password').click();
	});
});
