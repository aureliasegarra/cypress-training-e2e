describe('Login /logout Test', () => {
	beforeEach(() => {
		cy.visit('http://zero.webappsecurity.com/');
		cy.url().should('include', 'zero.webappsecurity.com/');
		cy.contains('Signin').click();
	});

	it('Should login with invalid datas and display error message', () => {
		cy.get('#login_form').should('be.visible');
		cy.get('#user_login').type('invalid username');
		cy.get('#user_password').type('invalid password');
		cy.contains('Sign in').click();
		cy.get('.alert-error')
			.should('be.visible')
			.and('contain', 'Login and/or password are wrong');
	});

	it('Should login with valid datas', () => {
		cy.fixture('user').then(user => {
			const username = user.id;
			const password = user.password;

			cy.get('#user_login').type(username);
			cy.get('#user_password').type(password);
			cy.get('#user_remember_me').click();
			cy.contains('Sign in').click();
		});
		cy.get('ul.nav-tabs').should('be.visible');
	});

	it('Should logout from application', () => {
		cy.fixture('user').then(user => {
			const username = user.id;
			const password = user.password;

			cy.get('#user_login').type(username);
			cy.get('#user_password').type(password);
			cy.get('#user_remember_me').click();
			cy.contains('Sign in').click();
		});
		cy.get('ul.nav-tabs').should('be.visible');
		cy.url().should('include', 'account-summary.html');
        
        cy.contains('username').click();
        cy.get('#logout_link').click();
        cy.url().should('include', 'index.html');
	});
});
