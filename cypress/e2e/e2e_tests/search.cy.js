describe('Searchbox test', () => {
	beforeEach(() => {
		cy.visit('http://zero.webappsecurity.com/');
	});

	it('Should type into searchbox and submit pressing enter', () => {
		cy.get('#searchTerm').type('Card {enter}');
	});
	it('Should show search results page', () => {
        cy.get('#searchTerm').type('Card {enter}');
		cy.get('h2').contains('Search Results:');
	});
});
